fullscreen();
$(window).resize(fullscreen);
$(window).scroll(headerParallax);

function fullscreen() {
	var masthead = $('.home-wrapper');
	var windowH = $(window).height();
	var windowW = $(window).width();

	masthead.width(windowW);
	masthead.height(windowH);
}

function headerParallax() {
	var st = $(window).scrollTop();
	var headerScroll = $('.home-content');

	if (st < 500) {
		headerScroll.css('opacity', 1-st/1000);
		headerScroll.css({
			'-webkit-transform' : 'translateY(' + st/8 + '%)',
			'-ms-transform' : 'translateY(' + st/8 + '%)',
			transform : 'translateY(' + st/8 + '%)'
		});
	}
}
$(document).on("scroll", function(){
  if
    ($(document).scrollTop() > 100){
    $(".menu").addClass("small");
  }
  else
  {
    $(".menu").removeClass("small");
  }
});
